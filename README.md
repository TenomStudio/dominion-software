### What is this repository for? ###

Demo for Dominion Sofware. It uses angular javascript framework, bootstrap, jquery, slick.

Live Demo: https://dominion-software.firebaseapp.com

### How do I get set up? ###
1. npm install
2. bower install

### To test locally ###
1. gulp dev
	- Refresh browser on changes in js, html, scss file.

### To build ###
1. gulp build 
	- This will compress images files
	- Translate scss to css
	- Minify js and css
	- Convert html templates to templatecache (in javascript)
	- Copy fonts from vendor

### To test built files ###
1. gulp prod
	- Serve index.html from build folder
	
### Who do I talk to? ###

Any trouble or question? Just email me at phangchuen.chan@outlook.com. Or we can meet at any cafe :)
