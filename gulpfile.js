var args = require('yargs').argv;
var gulp = require('gulp');
var sass = require('gulp-sass');
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var $ = require('gulp-load-plugins')({ lazy: true });
var del = require('del');
var browserSync = require('browser-sync').create();



// Copy third party libraries from /node_modules into /vendor
gulp.task('vendor', function () {

  // Bootstrap
  gulp.src([
    './node_modules/bootstrap/dist/**/*',
    '!./node_modules/bootstrap/dist/css/bootstrap-grid*',
    '!./node_modules/bootstrap/dist/css/bootstrap-reboot*'
  ])
    .pipe(gulp.dest('./assets/vendor/bootstrap'))

  // Font Awesome
  gulp.src([
    './node_modules/font-awesome/**/*',
    '!./node_modules/font-awesome/{less,less/*}',
    '!./node_modules/font-awesome/{scss,scss/*}',
    '!./node_modules/font-awesome/.*',
    '!./node_modules/font-awesome/*.{txt,json,md}'
  ])
    .pipe(gulp.dest('./assets/vendor/font-awesome'))

  // jQuery
  gulp.src([
    './node_modules/jquery/dist/*',
    '!./node_modules/jquery/dist/core.js'
  ])
    .pipe(gulp.dest('./assets/vendor/jquery'))

  // jQuery Easing
  gulp.src([
    './node_modules/jquery.easing/*.js'
  ])
    .pipe(gulp.dest('./assets/vendor/jquery-easing'))

});

// Compile SCSS
gulp.task('css:compile', function () {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass.sync({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'))
});

// Minify CSS
gulp.task('css:minify', ['css:compile'], function () {
  return gulp.src([
    './assets/css/*.css',
    '!./assets/css/*.min.css'
  ])
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./assets/css'))
    .pipe(gulp.dest('./.tmp'))
    .pipe(browserSync.stream());
});

// CSS
gulp.task('css', ['css:compile', 'css:minify']);

// Minify JavaScript
gulp.task('js:minify', function () {
  return gulp.src([
    './app/*.js',
    '!./app/*.min.js'
  ])
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    // .pipe(gulp.dest('./app/js'))
    .pipe(browserSync.stream());
});

// JS
gulp.task('js', ['js:minify']);

// Default task
gulp.task('default', ['css', 'vendor']);

// Configure the browserSync task
gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
});

// Dev task
gulp.task('dev', ['css', 'js', 'browserSync'], function () {
  gulp.watch('./assets/scss/*.scss', ['css']);
  gulp.watch('./app/**/*.js', ['js']);
  gulp.watch('./**/*.html', browserSync.reload);
});

gulp.task('prod', function() {
  browserSync.init({
    server: {
      baseDir: "./build"
    }
  });
})

gulp.task('inject', ['wiredep', 'css', 'templatecache'], function () {
  log('Wire up css into the html, after files are ready');
  var wiredep = require('wiredep').stream;
  var options = {
    bowerJson: require('./bower.json'),
    directory: './bower_components/',
    ignorePath: '..'
  };

  return gulp
    .src('./index.html')
    .pipe(wiredep(options))
    .pipe(inject('./.tmp/app.min.css')).pipe(gulp.dest('./'));
});

gulp.task('wiredep', function () {
  var js = [].concat(['app/**/*.module.js', 'app/**/*.js', '!app/**/*.spec.js', '!app/**/*.min.js']);
  return gulp
    .src('./index.html')
    .pipe(inject(js, '', [
      '**/app.module.js',
      '**/*.module.js',
      '**/*.js'
    ]))
    .pipe(gulp.dest('./'));
});



gulp.task('optimize', ['inject'], function () {
  log('Optimizing the js, css, and html');
  var assets = $.useref.assets({ searchPath: './' });
  var cssFilter = $.filter('**/*.css');
  var jsAppFilter = $.filter('**/app.js');
  var jslibFilter = $.filter('**/lib.js');
  var jsVendorFilter = $.filter('**/vendor.js');
  var templateCache = './.tmp/templates.js';

  //gulp.src(['./assets/vendor/**/*.*']).pipe(gulp.dest('./build/vendor'));

  return (gulp
    .src('./index.html')
    .pipe($.plumber())
    .pipe(inject(templateCache, 'templates'))
    .pipe(assets) // Gather all assets from the html with useref
    // Get the css
    .pipe(cssFilter)
    .pipe($.minifyCss())
    .pipe(cssFilter.restore())
    // Get the custom javascript
    .pipe(jsAppFilter)
    .pipe($.ngAnnotate({ add: true }))
    .pipe($.uglify())
    .pipe(getHeader())
    .pipe(jsAppFilter.restore())
    // Get the vendor javascript
    .pipe(jslibFilter)
    .pipe($.uglify())
    .pipe(jslibFilter.restore())
    .pipe(jsVendorFilter)
    .pipe($.uglify())
    .pipe(jsVendorFilter.restore())

    .pipe($.rev())
    // Apply the concat and file replacement with useref
    .pipe(assets.restore())
    .pipe($.useref())
    // Replace the file names in the html with rev numbers
    .pipe($.revReplace())
    .pipe(gulp.dest('./build/')));
})

gulp.task('templatecache', ['clean-code'], function () {
  log('Creating an AngularJS $templateCache');

  return gulp
    .src(['app/**/*.html'])
    .pipe($.if(args.verbose, $.bytediff.start()))
    .pipe($.minifyHtml({ empty: true }))
    .pipe($.if(args.verbose, $.bytediff.stop(bytediffFormatter)))
    .pipe($.angularTemplatecache('templates.js', {
      module: 'app.core',
      root: '/app',
      standalone: false
    }))
    .pipe(gulp.dest('./.tmp'));
});

gulp.task('clean-code', function (done) {
  var files = [].concat('./.tmp/**/*.js', './build/fonts/**/*.*' ,'./build/js/**/*.js','./build/styles/**/*.css' ,'./build/**/*.html');
  clean(files, done);
});

gulp.task('clean-images', function (done) {
  clean('./build/img/**/*.*', done);
});

gulp.task('images', ['clean-images'], function () {
  log('Compressing and copying images');
  return gulp.src('img/**/*.*').pipe($.imagemin({ optimizationLevel: 4 })).pipe(gulp.dest('./build/img'));
});

gulp.task('fonts', ['clean-fonts'], function () {
  log('Copying fonts');

  return gulp
    .src(['assets/fonts/**/*.*'])
    .pipe(gulp.dest('./build/fonts'));
});

gulp.task('clean-fonts', function (done) {
  clean('./build/fonts/**/*.*', done);
});


gulp.task('build', ['optimize', 'images', 'fonts'], function () {
  log('Building everything');
});


function clean(path, done) {
  log('Cleaning: ' + $.util.colors.blue(path));
  del(path, done);
}

function bytediffFormatter(data) {
  var difference = data.savings > 0 ? ' smaller.' : ' larger.';
  return (
    data.fileName +
    ' went from ' +
    (data.startSize / 1000).toFixed(2) +
    ' kB to ' +
    (data.endSize / 1000).toFixed(2) +
    ' kB and is ' +
    formatPercent(1 - data.percent, 2) +
    '%' +
    difference
  );
}

function inject(src, label, order) {
  var options = { read: false };
  if (label) {
    options.name = 'inject:' + label;
  }

  return $.inject(orderSrc(src, order), options);
}

function orderSrc(src, order) {
  //order = order || ['**/*'];
  return gulp.src(src).pipe($.if(order, $.order(order)));
}

function log(msg) {
  if (typeof msg === 'object') {
    for (var item in msg) {
      if (msg.hasOwnProperty(item)) {
        $.util.log($.util.colors.blue(msg[item]));
      }
    }
  } else {
    $.util.log($.util.colors.blue(msg));
  }
}

function getHeader() {
  var pkg = require('./package.json');
  var template = [
    '/**',
    ' * <%= pkg.name %> - <%= pkg.description %>',
    ' * @authors <%= pkg.authors %>',
    ' * @version v<%= pkg.version %>',
    ' * @link <%= pkg.homepage %>',
    ' * @license <%= pkg.license %>',
    ' */',
    ''
  ].join('\n');
  return $.header(template, {
    pkg: pkg
  });
}