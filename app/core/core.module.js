(function() {
    'use strict';
  
    angular
      .module('app.core', [
        'ui.router','blocks.exception', 'blocks.logger', 'blocks.router'
      ]);
  })();
  