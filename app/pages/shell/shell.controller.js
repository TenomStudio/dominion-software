(function () {
    'use strict';

    angular
        .module('app.pages')
        .controller('ShellController', ShellController);

    ShellController.$inject = [];
    function ShellController() {
        var vm = this;


        activate();

        ////////////////

        function activate() {

            // Collapse Navbar
            var navbarCollapse = function () {
                if ($("#mainNav").offset().top > 100) {
                    $("#mainNav").addClass("navbar-shrink");
                } else {
                    $("#mainNav").removeClass("navbar-shrink");
                }
            };
            // Collapse now if page is not at top
            navbarCollapse();
            // Collapse the navbar when page is scrolled
            $(window).scroll(navbarCollapse);

            $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                    $(this).toggleClass('open');
                }
            );

            var s = $('input'),
                f = $('form'),
                a = $('.after')

            s.focus(function () {
                $('.navbar-brand, .navbar-toggler').hide();
                if (f.hasClass('open')) return;
                f.addClass('in');
                setTimeout(function () {
                    f.addClass('open');
                    f.removeClass('in');
                    
                }, 1300);
            });

            a.on('click', function (e) {
                
                e.preventDefault();
                if (!f.hasClass('open')) return;
                s.val('');
                f.addClass('close');
                f.removeClass('open');
                setTimeout(function () {
                    f.removeClass('close');
                    $('.navbar-toggler, .navbar-brand').show();
                }, 1300);
            })


        }
    }
})();