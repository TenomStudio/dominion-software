(function () {
    'use strict';

    angular
        .module('app.pages.landing')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'pages-landing',
                config: {
                    url: '/',
                    templateUrl: '/app/pages/landing/landing.html',
                    controller: 'LandingController',
                    controllerAs: 'vm',
                    title: 'Welcome'
                }
            }
        ];
    }


})();
