(function () {
    'use strict';

    angular
        .module('app.pages.landing')
        .controller('LandingController', LandingController);

    LandingController.$inject = [];
    function LandingController() {
        var vm = this;
        
        activate();

        ////////////////

        function activate() {
            $('.product-modal').on('show.bs.modal', function (e) {
                $(".navbar").addClass("d-none");
            })
            $('.product-modal').on('hidden.bs.modal', function (e) {
                $(".navbar").removeClass("d-none");
            })
            checkScreenSize();
            $(window).on('resize', checkScreenSize);
            vm.products = [
                {
                    id: 'productModal1',
                    image: 'img/product/01-thumbnail.jpg',
                    fullImage: 'img/product/01-full.jpg',
                    title: 'Fashion',
                    description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
                },
                {
                    id: 'productModal2',
                    image: 'img/product/02-thumbnail.jpg',
                    fullImage: 'img/product/02-full.jpg',
                    title: 'Adventure',
                    description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
                },
                {
                    id: 'productModal3',
                    image: 'img/product/03-thumbnail.jpg',
                    fullImage: 'img/product/03-full.jpg',
                    title: 'Style',
                    description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
                },
                {
                    id: 'productModal4',
                    image: 'img/product/04-thumbnail.jpg',
                    fullImage: 'img/product/04-full.jpg',
                    title: 'Tech gadgets',
                    description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
                },
                {
                    id: 'productModal5',
                    image: 'img/product/05-thumbnail.jpg',
                    fullImage: 'img/product/05-full.jpg',
                    title: 'Modern',
                    description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
                },
                {
                    id: 'productModal6',
                    image: 'img/product/06-thumbnail.jpg',
                    fullImage: 'img/product/06-full.jpg',
                    title: 'Travel',
                    description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
                },
            ];
        }

        function checkScreenSize() {
            if ($(window).innerWidth() < 600 && vm.slicked !== true) {
                vm.slicked = true;
                setTimeout(function () {
                    $('#productsCarousel').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        dots: true,
                        centerMode: true
                    });
                });
            } else if ($(window).innerWidth() >= 600 && vm.slicked === true) {
                vm.slicked = false;
                $('#productsCarousel').slick('unslick');
            }

        }


    }

})();